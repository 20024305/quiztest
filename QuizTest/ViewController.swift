import UIKit

class ViewController: UIViewController {

    struct Question {
        let question : String
        let answers : Array<String>
        let positionCorrectAnswer : Int
    }

    var questions = Array<Question>()

    var currentElementIndex = 0

    @IBOutlet weak var questionLabel: UILabel!


    @IBOutlet weak var firstAnswer: UIButton!

    @IBOutlet weak var secondAnswer: UIButton!

    @IBOutlet weak var thirdAnswerButton: UIButton!

    var userScore: Int = 0
    
    let correctSound = SimpleSound(named:"Corretto")
    
    let wrongSound = SimpleSound(named:"Errore")
        
    override func viewDidLoad() {
        super.viewDidLoad()
        initQuestions()
    }

    func enableButtons(_ value :Bool){
        firstAnswer.isHidden = value
        secondAnswer.isHidden = value
        thirdAnswerButton.isHidden = value
    }

    func initQuestions(){
        //Prima domanda
        questions.append(Question(
            question: "Di che colore era il cavallo bianco di Napoleone?",
            answers: ["Bianco", "Nero", "Grigio"],
            positionCorrectAnswer: 1))
        
        //Seconda domanda
        questions.append(Question(
        question: "Quanti mesi hanno 28 giorni?",
        answers: ["Febbraio, Marzo e Luglio", "Tutti", "Febbraio"],
        positionCorrectAnswer: 2))
        
        //Terza Domanda
        questions.append(Question(
        question: "Se a 6 anni tuo fratello ha la metà dei tuoi anni, a 50 anni, quanti anni ha tuo fratello?",
        answers: ["25", "54", "47"],
        positionCorrectAnswer:3))
        
        //Quarta domanda
        questions.append(Question(
        question: "Da quanti bit è composto un Byte?",
        answers: ["1", "8", "16"],
        positionCorrectAnswer:2))
        
        //Quinta domanda
        questions.append(Question(
        question: "Quale dei seguenti è un dispositivo di input?",
        answers: ["Mouse", "Monitor", "Stampante"],
        positionCorrectAnswer:1))
        
        //Sesta domanda
        questions.append(Question(
        question: "Quale dei seguenti è un dispositivo di output?",
        answers: ["Scanner", "Casse audio", "Tastiera"],
        positionCorrectAnswer:2))
        
        //Settima domanda
        questions.append(Question(
        question: "Cosa significa WWW?",
        answers: ["World Web Wide", "Wire Web World", "World Wide Web"],
        positionCorrectAnswer:3))
        
        //Ottava domanda
        questions.append(Question(
        question: "Quanto fa 2^10?",
        answers: ["1000", "2000", "1024"],
        positionCorrectAnswer:3))
        
        //Nona domanda
        questions.append(Question(
        question: "Quale di questi è un linguaggio ad oggetti?",
        answers: ["Java", "C", "Html"],
        positionCorrectAnswer:1))
        
        //Decima domanda
        questions.append(Question(
        question: "Che cos'è un IDE",
        answers: ["Un editor di testo", "Un ambiente di sviluppo", "Un browser"],
        positionCorrectAnswer:2))
        
        
        nextQuestion()
    }


    func nextQuestion() {
        if(currentElementIndex < questions.count){
            let question = questions[currentElementIndex]
            
            questionLabel.text = question.question
            
            firstAnswer.setTitle(question.answers[0], for: .normal)
            
            secondAnswer.setTitle(question.answers[1], for: .normal)
            
            thirdAnswerButton.setTitle(question.answers[2], for: .normal)

            currentElementIndex = currentElementIndex+1
        }else{
            enableButtons(true)
            questionLabel.text = "User score : \(userScore)"
        }
    }

    @IBAction func checkAnswerFirst(_ sender: Any) {
        let index = currentElementIndex - 1
        if(questions[index].positionCorrectAnswer == 1){
            userScore = userScore + 1
            correctSound.play()
        }else{
            wrongSound.play()
        }
        nextQuestion()

    }

    @IBAction func checkAnswerSecond(_ sender: Any) {
        let index = currentElementIndex - 1
        if(questions[index].positionCorrectAnswer == 2){
            userScore = userScore + 1
            correctSound.play()
        }else{
            wrongSound.play()
        }
        nextQuestion()
        
    }

    @IBAction func checkAnswerThird(_ sender: Any) {
        let index = currentElementIndex - 1
        if(questions[index].positionCorrectAnswer == 3){
            userScore = userScore + 1
            correctSound.play()
        }else{
            wrongSound.play()
        }
        nextQuestion()
    }
}

